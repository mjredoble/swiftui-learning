//
//  ItemRowView.swift
//  iDine
//
//  Created by Michael Joseph Redoble on 13/04/2021.
//

import SwiftUI

struct ItemRowView: View {
    let colors: [String: Color] = ["D": .purple, "G": .black, "N": .red, "S": .blue, "V": .green]
    let item: MenuItem
    
    var body: some View {
        HStack {
            //A circle image view with overlay
            Image(item.thumbnailImage)
                .clipShape(Circle())
                .overlay(Circle().stroke(Color.gray, lineWidth: 2))
            
            VStack(alignment: .leading) {
                Text(item.name)
                    .font(.headline)
                Text("$\(item.price)")
            }
            
            //This will automatically take up spaces.
            Spacer()
            
            ForEach(item.restrictions, id: \.self) { restriction in
                //More UI modifiers
                Text(restriction)
                    .font(.caption)
                    .fontWeight(.black)
                    .padding(5)
                    .background(colors[restriction, default: .black])
                    .foregroundColor(Color.white)
                    .clipShape(Circle())
                    
            }
        }
    }
}

struct ItemRowView_Previews: PreviewProvider {
    static var previews: some View {
        //Sample image is provided for preview
        ItemRowView(item: MenuItem.example)
    }
}
