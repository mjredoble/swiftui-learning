//
//  SwiftUIView.swift
//  iDine
//
//  Created by Michael Joseph Redoble on 07/05/2021.
//

import SwiftUI

struct ItemDetailView: View {
    @EnvironmentObject var order: Order
    
    let item: MenuItem
    
    var body: some View {
        VStack {
            //Zstack with alignment
            ZStack(alignment: .bottomTrailing) {
                
                //Image view with ui modifiers to resize and scale base on device because SwiftUI displays images at their natural size by default we add .resizable() and .scaledToFit()
                
                Image(item.mainImage)
                    .resizable()
                    .scaledToFit()
                Text("Photo: \(item.photoCredit)")
                    .padding(4)
                    .font(.caption)
                    .background(Color.black)
                    .foregroundColor(.white)
                    .offset(x: -5, y: -5)
            }
            Text(item.description)
                .padding()
            
            Button("Order This") {
                order.add(item: item)
            }
            .font(.headline)
            
            Spacer()
        }
        .navigationTitle(item.name)
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        ItemDetailView(item: MenuItem.example)
            .environmentObject(Order())
    }
}
