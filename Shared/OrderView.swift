//
//  OrderView.swift
//  iDine
//
//  Created by Michael Joseph Redoble on 16/05/2021.
//

import SwiftUI

struct OrderView: View {
    @EnvironmentObject var order: Order

    //TEST COMMIT
    var body: some View {
        NavigationView {
            List {
                Section {
                    ForEach(order.items) { item in
                        HStack {
                            Text(item.name)
                            Spacer()
                            Text("$\(item.price)")
                        }
                    }
                    .onDelete(perform: deleteItems)
                }
                
                
                Section {
                    NavigationLink(destination: CheckoutView()) {
                        Text("Place Order")
                    }
                }
            }
            .navigationTitle("Order")
            .listStyle(InsetGroupedListStyle())
            .toolbar(content: {
                //Add an Edit toolbar button on top right for DELETE ROW
                EditButton()
            })
        }
    }
    
    func deleteItems(at offset: IndexSet) {
        order.items.remove(atOffsets: offset)
    }
}

struct OrderView_Previews: PreviewProvider {
    static var previews: some View {
        OrderView()
            .environmentObject(Order())
    }
}
