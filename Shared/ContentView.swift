//
//  ContentView.swift
//  Shared
//
//  Created by Michael Joseph Redoble on 13/04/2021.
//

import SwiftUI

struct ContentView: View {
    @EnvironmentObject var order: Order
    
    let menu = Bundle.main.decode([MenuSection].self, from: "menu.json")
    
    var body: some View {
        NavigationView {
            List {
                ForEach(menu) {section in
                    Section(header: Text(section.name)) {
                        
                        //THIS IS CELLS INSIDE SECTION
                        ForEach(section.items) {menu in
                            NavigationLink(destination: ItemDetailView(item: menu)) {
                                ItemRowView(item: menu)
                            }
                        }
                    }
                }
            }
            .listStyle(GroupedListStyle())
            .navigationTitle("Hello World!")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
