//
//  iDineApp.swift
//  Shared
//
//  Created by Michael Joseph Redoble on 13/04/2021.
//

import SwiftUI

@main
struct iDineApp: App {
    //@StateObject property wrapper is responsible for keeping the object alive throughout the life of our app.
    @StateObject var order = Order()
    
    var body: some Scene {
        WindowGroup {
            MainView()
                .environmentObject(order)
        }
    }
}
